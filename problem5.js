function groupByLanguages(users) {
  if (typeof users === "object") {
    let groupByLanguages = Object.keys(users).reduce((acc, user) => {
      let userObj = users[user];
      let languages = ["Golang", "Javascript", "Python"];
      let desgination = userObj.desgination;
      let language = languages.filter((lang) => desgination.includes(lang));
      if (acc[language]) {
        acc[language] = [...acc[language], user];
      } else {
        acc[language] = [user];
      }
      return acc;
    }, {});
    return groupByLanguages;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = groupByLanguages;
