function livingInGermany(users) {
  if (typeof users === "object") {
    let livingInGermany = Object.keys(users).filter((user) => {
      let obj = users[user];
      if (obj.nationality === "Germany") return true;

      return false;
    });
    return livingInGermany;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = livingInGermany;
