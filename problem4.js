function mastersStudents(users) {
  if (typeof users === "object") {
    let mastersStudents = Object.keys(users).filter((user) => {
      return users[user].qualification === "Masters";
    });
    return mastersStudents;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = mastersStudents;
