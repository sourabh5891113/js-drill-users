function interestedInVideoGames(users) {
  if (typeof users === "object") {
    let interestedInVideoGames = Object.keys(users).filter((user) => {
      let obj = users[user];
      let interests = obj.interests || obj.interest;
      for (let key of interests) {
        if (key.includes("Video Games")) return true;
      }
      return false;
    });
    return interestedInVideoGames;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = interestedInVideoGames;
