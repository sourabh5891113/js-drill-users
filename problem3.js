function sortByDesginationThenAge(users) {
  if (typeof users === "object") {
    function compareSeniority(userOne, userTwo) {
      const seniorityOrder = {
        Senior: 3,
        Developer: 2,
        Intern: 1,
      };
      let user1 = users[userOne];
      let user2 = users[userTwo];
      let ageA = user1.age;
      let ageB = user2.age;
      let user1Arr = user1.desgination.split(" ");
      let user2Arr = user2.desgination.split(" ");
      const user1Seniority =
        seniorityOrder[user1Arr[0]] || seniorityOrder[user1Arr[1]];
      const user2Seniority =
        seniorityOrder[user2Arr[0]] || seniorityOrder[user2Arr[1]];
      if (user2Seniority - user1Seniority === 0) {
        return ageB - ageA;
      }
      return user2Seniority - user1Seniority;
    }
    let sortByDesginationThenAge = Object.keys(users).sort(compareSeniority);
    return sortByDesginationThenAge;
  } else {
    console.log("First argument should be an object");
    return [];
  }
}

module.exports = sortByDesginationThenAge;
